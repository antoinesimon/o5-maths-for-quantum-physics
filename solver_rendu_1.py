import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np


def psi0(x):
    # Gaussian function not centered on 0
    L = 10
    sigma = L/20
    mu = -L/4
    k = 15.
    return (1/(sigma*np.sqrt(2*np.pi))) * np.exp(-1/2*(x-mu)**2/sigma**2) * np.exp(1j*k*x)

def psi0_0(x):
    # Gaussian function centered on 0
    # Used for heat equation we should see the temperature smoothly reach a constant value over
    # the whole domain (diffusion through the material, no loss / gain of energy)
    L = 10
    sigma = L/20
    return (1/(sigma*np.sqrt(2*np.pi))) * np.exp(-1/2*x**2/sigma**2)

def psi0_1(x):
    # Cosine that should be a stationary wave
    L = 10
    return np.cos(2*np.pi*5*x/L)

def psi0_2(x):
    # Hyperbolic cosine on which we should see something like two wave packets, initially
    # localized on the edges of the domain, interfer
    return np.cosh(x)

def vec_barrier(width=1., energy=5.):
    def barrier(x, t):
        if x >= -width/2 and x <= width/2:
            return energy
        return 0.
    return np.vectorize(barrier, excluded=['t'])


# Simulates the Schrödinger dynamics i∂_tψ = -1/2 ψ'' + V(x,t) ψ, with the pseudospectral method
# on an interval [-L/2,L/2] with periodic boundary conditions, with Nx grid points
# The simulation proceeds from 0 to T, with Nt time steps.
# Initial condition is given by the function psi0_fun(x), potential by the function V_fun(x,t)
# Returns an array psi[ix, it]
"""def dynamics(psi0_fun=(lambda x: np.exp(-x**2)), V_fun=(lambda x,t: 0), L=10, Nx=1000, T=4, Nt=1000):
    X = -L/2 + (L/Nx)*np.arange(Nx)
    dt = T/Nt

    kinetic_energy_1 = -1/2 * np.array([0] + [-((2*k*np.pi/L)**2) for k in range(1, Nx//2+1)] + [-((2*l*np.pi/L)**2) for l in range(-(Nx-1-Nx//2), 0)])
    
    psi_0 = psi0_fun(X)
    f_psi_0 = np.fft.fft(psi_0)

    f_psi_last = f_psi_0
    psi_list = [psi_0]

    for n in range(Nt):
        f_psi_last = np.exp(-1j*kinetic_energy_1*dt) * np.exp(-1j*V_fun(X, n*dt)*dt) * f_psi_last
        psi_t = np.fft.ifft(f_psi_last)
        psi_list.append(psi_t)

    return np.transpose(np.array(psi_list))"""
def dynamics(psi0_fun=(lambda x: np.exp(-x**2)), V_fun=(lambda x,t: 0), L=10, Nx=1000, T=4, Nt=1000):
    X = -L/2 + (L/Nx)*np.arange(Nx)
    dt = T/Nt

    kinetic_energy_1 = -1/2 * np.array([0] + [-((2*k*np.pi/L)**2) for k in range(1, Nx//2+1)] + [-((2*l*np.pi/L)**2) for l in range(-(Nx-1-Nx//2), 0)])
    
    psi_0 = psi0_fun(X)

    psi_list = [psi_0]

    for n in range(Nt):
        psi_t = np.fft.ifft(np.exp(-1j*kinetic_energy_1*dt) * np.fft.fft(np.exp(-1j*V_fun(X, n*dt)*dt) * psi_list[-1]))
        psi_list.append(psi_t)

    return np.transpose(np.array(psi_list))


# Simulates the heat dynamics i∂t = -1/2 ψ'' + V(x,t) ψ, with the pseudospectral method
# on an interval [-L,L] with periodic boundary conditions, with Nx grid points
# The simulation proceeds from 0 to T, with Nt time steps.
# Initial condition is given by the function psi0_fun(x), potential by the function V_fun(x,t)
# Returns an array psi[ix, it]
def dynamics_heat(psi0_fun=(lambda x: np.exp(-x**2)), V_fun=(lambda x,t: 0), L=10, Nx=1000, T=4, Nt=1000):
    X = -L/2 + (L/Nx)*np.arange(Nx)
    dt = T/Nt

    kinetic_energy_1 = -1/2 * np.array([0] + [-((2*k*np.pi/L)**2) for k in range(1, Nx//2+1)] + [-((2*l*np.pi/L)**2) for l in range(-(Nx-1-Nx//2), 0)])
    
    psi_0 = psi0_fun(X)
    f_psi_0 = np.fft.fft(psi_0)

    f_psi_last = f_psi_0
    psi_list = [psi_0]

    for n in range(Nt):
        f_psi_last = np.exp(-kinetic_energy_1*dt) * np.exp(-V_fun(X, n*dt)*dt) * f_psi_last
        psi_t = np.fft.ifft(f_psi_last)
        psi_list.append(psi_t)

    return np.transpose(np.array(psi_list))


# Plots the return value psi of the function "dynamics", using linear interpolation
# The whole of psi is plotted, in an animation lasting "duration" seconds (duration is unconnected to T)
# L argument is only for x axis labelling
def plot_psi(psi, duration=10, frames_per_second=30, L=10):
    fig, ax = plt.subplots()
    t_data = np.linspace(0, 1, np.size(psi, 1)) # 1 is arbitrary here
    x_data = np.linspace(-L/2,L/2,np.size(psi,0), endpoint=False)
    # set the min and maximum values of the plot, to scale the axis
    m = min(0, np.min(np.real(psi)), np.min(np.imag(psi)))
    M = np.max(np.abs(psi))
    # set the axis once and for all
    ax.set(xlim=[-L/2,L/2], ylim=[m,M], xlabel='x', ylabel='psi')
    # dummy plots, to update during the animation
    real_plot = ax.plot(x_data, np.real(psi[:, 0]), label='Real')[0]
    imag_plot = ax.plot(x_data, np.imag(psi[:, 0]), label='Imag')[0]
    abs_plot  = ax.plot(x_data, np.abs(psi[:, 0]), label='Abs')[0]
    ax.legend()

    # define update function as an internal function (that can access the variables defined before)
    # will be called with frame=0...(duration*frames_per_second)-1
    def update(frame):
        print(frame)
        # get the data by linear interpolation
        t = frame / (duration * frames_per_second)
        psi_t = np.array([np.interp(t, t_data, psi[i, :]) for i in range(np.size(psi,0))])
        # update the plots
        real_plot.set_ydata(np.real(psi_t))
        imag_plot.set_ydata(np.imag(psi_t))
        abs_plot.set_ydata(np.abs(psi_t))

    ani = animation.FuncAnimation(fig=fig, func=update, frames=duration*frames_per_second, interval=1000/frames_per_second)
    return ani


# psi = dynamics_heat(psi0_fun=psi0, T=12, Nt=3000)
# psi = dynamics(psi0_fun=psi0_2, T=12, Nt=4000)
# ani = plot_psi(psi)

# ani.save("ani-7.gif")
"""
if __name__ == "__main__":
    ## First : the centered Gaussian that goes to constant heat
    psi_1 = dynamics_heat(psi0_fun=psi0_0, T=12, Nt=3000)
    ani_1 = plot_psi(psi_1)
    ani_1.save("heat_equation_gaussian.gif")

    ## Second : the stationary plane wave
    psi_2 = dynamics(psi0_fun=psi0_1, T=8, Nt=2000)
    ani_2 = plot_psi(psi_2)
    ani_2.save("schrodinger_cosine.gif")

    ## Third : the interferences between two wave packets
    psi_3 = dynamics(psi0_fun=psi0_2, T=8, Nt=2000)
    ani_3 = plot_psi(psi_3)
    ani_3.save("schrodinger_hyperbolic_cosine.gif")

    ## Fourth : the non-centered Gaussian wave packet that propagates
    psi_4 = dynamics(psi0_fun=psi0, T=8, Nt=2000)
    ani_4 = plot_psi(psi_4)
    ani_4.save("schrodinger_gaussian.gif")
"""

if __name__ == "__main__":
    ## Fourth : the non-centered Gaussian wave packet that propagates
    psi_4 = dynamics(psi0_fun=psi0, V_fun=vec_barrier(width=1., energy=20.), T=4, Nt=8000)
    ani_4 = plot_psi(psi_4)
    ani_4.save("test8.gif")

